(*
This problem is to find an arrangement of N queens on a chess board, such that no queen can attack any other queens on the board.
The chess queens can attack in any direction as horizontal, vertical, horizontal and diagonal way.
*)

MODULE Queens;
 IMPORT Out;
 VAR n, k: INTEGER;
     x: ARRAY 8 OF INTEGER;
     col: ARRAY 8 OF BOOLEAN;
     up, down: ARRAY 15 OF BOOLEAN;
(*
  PROCEDURE Write();
  VAR k: INTEGER;
  BEGIN 
     FOR k := 0 TO 6 DO Out.Int(x[k], 4)
  END ; 
     Out.Ln()
 END Write;
*)

 PROCEDURE generate(i: INTEGER);
  VAR h: INTEGER;
      q: BOOLEAN;
  BEGIN
  h := 0;
  REPEAT q := FALSE; 
    IF col[h] & up[i+h] & down[i-h+7] THEN
       x[i] := h;
       col[h] := FALSE; up[i+h] := FALSE; down[i-h+7] := FALSE;
       IF i < 7 THEN
           generate(i+1);
          IF ~q THEN
             col[h] := TRUE; up[i+h] := TRUE; down[i-h+7] := TRUE
          END 
       ELSE q := TRUE
       END
    END ;
    INC(h)
  UNTIL q OR (h = 8)
 END generate;

 PROCEDURE Init();
  VAR n: INTEGER;
  BEGIN
  FOR n := 0 TO 7 DO
      col[n] := TRUE
  END;
  FOR n := 0 TO 14 DO
      up[n] := TRUE;
      down[n] := TRUE;
  END;
  generate(0);
 END Init;

 BEGIN
 Init
END Queens.
