MODULE Neumann;
  IMPORT In, Out, Math;
  CONST MAXLEN = 16;
  TYPE SeedList = ARRAY MAXLEN OF INTEGER;
  VAR seed, seedSquare, i, num, len, length: INTEGER;
      seedlist: SeedList;

   PROCEDURE getLen(seed: INTEGER): INTEGER;
   BEGIN
   len := 1;
   WHILE seed > 9 DO
         len := len + 1;
         seed := seed DIV 10;
   END;
   RETURN len;
   END getLen;

   PROCEDURE fillTheList(seed: INTEGER);
       BEGIN
       i := 0;
       len := getLen(seed);
       IF len < MAXLEN THEN
          length := (MAXLEN - len) DIV 2;
                WHILE seed > 0 DO
                        FOR i := 0 TO SHORT(LEN(SeedList, 0) - length) -  1 DO
                        num := seed MOD 10;
                        seedlist[length+i] := num;
                        seed := seed DIV 10;
                        END;
                END;
      END;
    Out.String("GENERATED NUMBER"); Out.Ln;
    FOR i := 6 TO MAXLEN - 7 DO
        Out.Int(seedlist[i], 0);
        Out.String(" ");
    END;
    Out.Ln;
   END fillTheList;

  BEGIN
    Out.String("seed = "); 
    In.Int(seed); Out.Ln;
    seedSquare := seed * seed;
    fillTheList(seedSquare);
END Neumann.
