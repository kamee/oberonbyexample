(*
The mission is to move all the disks to some another tower without violating the sequence of arrangement.
Rules:
    Only one disk can be moved at a time.
    Each move consists of taking the upper disk from one of the stacks and placing it on top of another stack or on an empty rod.
    No larger disk may be placed on top of a smaller disk.
*)

MODULE Towers;
 VAR discs: INTEGER;
 PROCEDURE TowerOfHanoi(discs: INTEGER; from, to, aux: CHAR);
 BEGIN
  IF discs # 1 THEN
    TowerOfHanoi(discs-1, from, aux, to);
    TowerOfHanoi(discs-1, aux, to, from);
  END;
 END TowerOfHanoi;

 BEGIN
 discs := 4;
 TowerOfHanoi(discs, 'A', 'B', 'C');
END Towers.
