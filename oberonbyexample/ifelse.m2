MODULE ifelse;
 IMPORT Texts, Oberon;
 VAR i: INTEGER;
 W: Texts.Writer;
 BEGIN Texts.OpenWriter(W);
 i := 3;
 IF i = 2 THEN
	Texts.WriteInt(W, i, 0);
	Texts.WriteLn(W)
 ELSIF i > 2 THEN
	Texts.WriteInt(W, i, 0);
	Texts.WriteLn(W)
 ELSE
	Texts.WriteString(W, 'i#2 or i > 1');
	Texts.WriteLn(W)
 END;
 Texts.Append(Oberon.Log, W.buf);
END ifelse.
