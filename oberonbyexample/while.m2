MODULE while;
 (*
WhileStatement =
		"WHILE" expression "DO" StatementSequence
		{"ELSIF" expression "DO" StatementSequence}
		END ;
 *)

 IMPORT Oberon, Texts;
 VAR i, num1, num2, num3: INTEGER;
 W: Texts.Writer;
 BEGIN
 Texts.OpenWriter(W);
 i := 1;
 num1 := 4;
 num3 := 12;
 WHILE i <= 3 DO
	Texts.WriteInt(W, i, 2);
	i := i + 1;
	Texts.WriteLn(W);
 END ;
 Texts.Append(Oberon.Log, W.buf);
END while.
