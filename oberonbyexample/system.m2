(*
SYSTEM contains procedures necessary to implement low-level operations.
*)
MODULE system;
 IMPORT SYSTEM, Out;
 PROCEDURE Union*(a, b: LONGINT): LONGINT;
  VAR s, t: SET;
 BEGIN
  s := SYSTEM.VAL(SET, a);
  t := SYSTEM.VAL(SET, b);
  RETURN SYSTEM.VAL(LONGINT, s + t)
 END Union;

 PROCEDURE Do*;
  VAR x, y: LONGINT;
  BEGIN
	x := 2;
	y := 4;
	x := Union(x, y);
	Out.Int(x, 0); Out.Ln
 END Do;
END system.
