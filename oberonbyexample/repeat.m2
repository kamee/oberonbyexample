(*
RepeatStatement =
		"REPEAT" StatementSequence "UNTIL" expression
 *)
(*
MODULE repeat;
 IMPORT Oberon, Texts;
 VAR i, num1, num2: INTEGER;
 W: Texts.Writer;
 BEGIN
 Texts.OpenWriter(W);
 num1 := 2;
 num2 := 0;
 REPEAT
  num1 := num1 + 1;
  Texts.WriteInt(W, num2, 6);
 UNTIL num1 = num2;
 Texts.Append(Oberon.Log, W.buf);
END repeat.
*)

MODULE repeat;
 IMPORT In, Out;
 VAR input: INTEGER;
 PROCEDURE isPrime(num: INTEGER): BOOLEAN;
  VAR divisor, remainder: INTEGER;
 BEGIN
  remainder := 0;
  IF num > 0 THEN
	divisor := 2;
	REPEAT
		remainder := num MOD divisor;
		divisor := divisor + 1
	UNTIL (remainder = 0) OR (divisor >= num)
  END;
  RETURN (remainder # 0)
 END isPrime;

 BEGIN
	In.Open;
	In.Int(input);
	IF In.Done THEN
		Out.Int(input, 0);
		IF isPrime(input) THEN
			Out.String(" is prime ")
		ELSE
			Out.String(" is not prime ")
		END;
	ELSE
		Out.String("invalid input: can't read number")
	END;
	Out.Ln;
END repeat.
