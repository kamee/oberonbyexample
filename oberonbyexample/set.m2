MODULE set;
 IMPORT Out;
 VAR first, second, third, fourth: SET; i, each: INTEGER;
 BEGIN
 	first := {1, 2, 3, 4};
	second := {4, 5, 6, 7};
(*
	Out.String("first:");
	FOR i := 0 TO MAX(SET) DO
		IF i IN first THEN
			Out.Int(i, 0); Out.String(" ");
		END;
	END;
	Out.Ln;
	*)
(*
	FOR i := 0 TO MAX(SET) DO
		IF first = second THEN
			Out.String("first = second"); Out.Ln;
		ELSE
			Out.String("first # second"); Out.Ln;
		END;
	END;
*)
	Out.String("third set: ");
	third := first + second;
	FOR i := 0 TO MAX(SET) DO
		IF i IN third THEN
			Out.Int(i, 0); Out.String(" ");
		END;
	END;
	Out.Ln;

	Out.String("fourth set: ");
	fourth := first * second;
	FOR i := 0 TO MAX(SET) DO
		IF i IN fourth THEN
			Out.Int(i, 0); Out.String(" ");
		END;
	END;
	Out.Ln;
END set.
