MODULE input;
 IMPORT In, Out;
 VAR age: INTEGER;
 BEGIN
	In.Int(age);
	Out.String("Age = "); Out.Int(age, 0); Out.Ln;
END input.
