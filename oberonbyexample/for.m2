(*
ForStatement = 
		FOR" identifier ":=" expression "TO" expression ["BY" ConstExpression] "DO"
			StatementSequence
		"END"
*)

MODULE for;
 IMPORT Texts, Oberon;
 CONST N = 3;
 VAR i: INTEGER;
 W: Texts.Writer;
 BEGIN
 Texts.OpenWriter(W);
 FOR i := 1 TO 10 DO
   i := 1 + i;
   Texts.WriteInt(W, i, 6); Texts.WriteLn(W);
 END ;
 Texts.Append(Oberon.Log, W.buf);
END for.
