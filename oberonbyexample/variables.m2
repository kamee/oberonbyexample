MODULE Variables;
 IMPORT Oberon, Texts;
 VAR a, b, c: INTEGER;
 c1: REAL;
 name: ARRAY 12 OF CHAR;
 W: Texts.Writer;
 BEGIN
 Texts.OpenWriter(W);
 a := 1;
 b := 2;
 c := a + b;
 c1 := b DIV a;
 name := 'kamee';
 Texts.WriteString(W, "a + b = "); Texts.WriteInt(W, c, 0); Texts.WriteLn(W);
 Texts.WriteString(W, "b / a = "); Texts.WriteReal(W, c1, 2); Texts.WriteLn(W);
 Texts.WriteString(W, "5 + 5 = "); Texts.WriteInt(W, 5 + 5, 2); Texts.WriteLn(W);
 Texts.WriteString(W, name); Texts.WriteLn(W);
 Texts.Append(Oberon.Log, W.buf);
END Variables.
