MODULE recordExtension;
 IMPORT Out;
 TYPE Car = RECORD
 	year-: INTEGER;
	make-: ARRAY 12 OF CHAR;
	price-: REAL;
 END;

 TYPE LuxuryCar = RECORD (Car)
 	packageCode-: INTEGER;
 END;
 
 VAR CarA: Car;
 	luxCarA: LuxuryCar;

 BEGIN
	CarA.year := 4;
	CarA.price := 4.5;
	luxCarA.packageCode := 3;

END recordExtension.
