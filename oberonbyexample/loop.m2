MODULE loop;
 IMPORT In, Out;
 PROCEDURE count;
 VAR count: INTEGER; ch: CHAR;
 BEGIN
  In.Open;
  count := 0;
  LOOP
  	In.Char(ch);
	IF ~In.Done THEN EXIT END;
	INC(count)
  END;
  Out.Int(count, 0);
  Out.String(" characters read"); Out.Ln;
  END count;
END loop.
