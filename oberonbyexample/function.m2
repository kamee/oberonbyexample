MODULE procedure;
 IMPORT Out, Texts, Oberon;
 VAR sum, product: INTEGER;
 PROCEDURE Product(first, last: INTEGER): INTEGER;
 BEGIN
	product := first * last;
	RETURN product;
 END Product;
 
 PROCEDURE Sum(first, last: INTEGER): INTEGER;
 BEGIN
 	sum := first + last;
	RETURN sum;
 END Sum;

 PROCEDURE Initials(first: CHAR): CHAR;
 BEGIN
  RETURN first;
 END Initials;
 BEGIN
 Out.Int(Product(1, 1), 0); Out.Ln;
 Out.Int(Product(3, 2), 0); Out.Ln;
 Out.Int(Sum(2, 4), 0); Out.Ln;
 Out.Char(Initials('c')); Out.Ln;
END procedure.
