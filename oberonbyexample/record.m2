MODULE record;
 IMPORT Out;

 TYPE
 	Driver = RECORD
		name: ARRAY 20 OF CHAR;
		age: INTEGER;
	END;

 TYPE
 	Car = RECORD
		info: Driver;
		year: INTEGER;
		make: ARRAY 12 OF CHAR;
		price: REAL;
		name: ARRAY 12 OF CHAR;
	END;

 VAR carA, carB: Car;

 BEGIN
 carA.info.name := 'name';
 carA.info.age := 20;
 carA.year := 1996;
 carA.make := 'Ford';
 carA.price := 2545.0;

 carB := carA;

 Out.String("Car A: driver, year, make, price, name:"); Out.Ln;
 Out.String(carA.info.name); Out.Ln;
 Out.Int(carA.info.age, 0); Out.Ln;
 Out.Int(carA.year, 0); Out.Ln;
 Out.String(carA.make); Out.Ln;
 Out.Real(carA.price, 0); Out.Ln;
 Out.String(carA.name); Out.Ln; (*empty string*)

 Out.String("Car B: year, make, price, name:"); Out.Ln;
 Out.String(carB.info.name); Out.Ln;
 Out.Int(carB.info.age, 0); Out.Ln;
 Out.Int(carB.year, 0); Out.Ln;
 Out.String(carB.make); Out.Ln;
 Out.Real(carB.price, 0); Out.Ln;
 Out.String(carB.name); Out.Ln; (*empty string*)

END record.
