(*
 CASE expression OF
 	<1st label>: <1st statement>
	|<2nd label>: <2nd statement
 ELSE
 	<default statements>
 END
*)
(*INPUT*)
MODULE switch;
 IMPORT In, Out;
 VAR ch: CHAR;
 countVowels: INTEGER;
 BEGIN
 In.Open;
 countVowels := 0;
 LOOP
 	In.Char(ch);
	IF ~In.Done THEN EXIT END;
 CASE ch OF
		"a", "e", "i", "o", "u": INC(countVowels);
 ELSE
 END;
 END;
 Out.Int(countVowels, 0);
 Out.String('vowels read'); Out.Ln;
END switch.

(* second version *)
(*
MODULE switch;
 IMPORT Texts, Oberon;
 VAR ch: CHAR;
 W: Texts.Writer;
 BEGIN Texts.OpenWriter(W);
	 ch := 'a';
	 CASE ch OF
		"a", "e", "i", "o", "u": Texts.WriteString(W, "vowel"); Texts.WriteLn(W);
	  ELSE
	 END;
 Texts.Append(Oberon.Log, W.buf);
END switch.
*)
