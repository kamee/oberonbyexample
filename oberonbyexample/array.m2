MODULE array;
 IMPORT Out;
 CONST
 	maxColumns = 27;
	maxRows = 5;
 TYPE StringTable = ARRAY maxRows, maxColumns OF CHAR;
 VAR str: StringTable; i: INTEGER;
 BEGIN
	str[0] := 'hi';
	str[1] := 'bye';
	FOR i := 0 TO SHORT(LEN(StringTable, 0)) -  1 DO
		Out.String(str[i]); Out.Ln
	END;
END array.
