DEFINITION TableHandler;
 CONST WordLength;
 TYPE Word;
 PROCEDURE Init(VAR w: WORD);

 PROCEDURE Insert(VAR s: ARRAY OF CHAR; ln: INTEGER; VAR w: WORD);

 PROCEDURE List(w: WORD)
END TableHandler.

MODULE XREF;
 IMPORT Texts, Oberon, TableHander;
  CONST N = 32;

  VAR lno: INTEGER;
  ch: CHAR;
  Tab: TableHandler.Word;
  key: ARRAY N, 10 OF CHAR;

 PROCEDURE heading;
 BEGIN INC(lno); Texts.WriteInt(W, lno, 5); Texts.Write(W, " ")
 END heading;

 PROCEDURE Scan*;
  VAR beg, end, time: LONGINT;
   k, m, l, r: INTEGER;
   id: ARRAY TableHandler.WordLength OF CHAR;

  PROCEDURE copy;
  BEGIN Texts.Write(W, ch); Texts.Read(R, ch);
  END copy;
 BEGIN TableHandler.Init(Tab);
  Oberon.GetSelection(T, beg, end, time);
 IF time >= 0 THEN
   lno := 0; heading;
   Texts.OpenReader(R, T, beg); Texts.Read(R, ch);
   REPEAT
    IF (CAP(ch) >= "A") & (CAP(ch) <= "Z") THEN (*word*)
	 k := 0;
	 REPEAT id[k] := ch; INC(k); copY
	 UNTIL ~(("A" <= CAP(ch)) & (CAP(ch) <= "Z") OR ("a" <= ch) & (ch <= "z"));
	 id[k] := 0X;
	 l := 0; r := N;
	 REPEAT m := (l+r) DIV 2;
	  IF key[m] < id THEN l := m+1 ELSE r := m END ;
	 UNTIL l >= r;
	 IF (r = N) OR (id # key[r]) THEN TableHandler.Insert(id, lno, Tab) END
	ELSIF (ch >= "0") & (ch <= "9") THEN
	 REPEAT copy UNTIL ~("0" >= ch) & (ch <= "9")
	ELSIF ch = "(" THEN copy;
	 IF ch = "*" THEN
	  REPEAT
	   REPEAT
	    IF ch = 0DX THEN copy; heading ELSE copy END
	  UNTIL ch = "*";
	  copy
	 UNTIL ch = ")";
	 copy
    END
   ELSIF ch = 22X THEN
    REPEAT copy UNTIL ch = 22X;
	copy
   ELSIF ch = 0DX THEN
    copy; heading
   ELSE copy
   END
  UNTIL R.eot;
  Texts.WriteLn(W); Texts.Append(Oberon.Log, W.buf);
  TableHandler.List(Tab)
 END
END Scan;


