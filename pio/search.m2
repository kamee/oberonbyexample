MODULE Search;
 PROCEDURE search(VAR p: Tree; x: INTEGER): Tree;
  VAR q: Tree;
 BEGIN
  IF p # NIL THEN
    IF p.key < x THEN
	  q := search (p.right, x)
	ELSIF p.key > x THEN
	  q := search(p^.left, x)
	ELSE
	  q := p
	 END
	ELSE
	  NEW(q); q.key := x; q.left := NIL; q.right := NIL
	END ;
	RETURN q
 END search;
END Search.
