MODULE gcd;
	PROCEDURE Gcd*;
		VAR x, y: INTEGER; S: Texts.Scanner;
	BEGIN Texts.OpenScanner(S, Oberon.Par.text, Oberon.Par.pos);
		Texts.Scan(S); x := S.i; Texts.WriteString(W, " x= "); Texts.WriteInt(W, x, 6);
		Texts.Scan(S); y := S.i; Texts.WriteString(W, " y = "); Texts.WriteInt(W, y, 6);
		u := x; v := y;
		WHILE x # y DO
			IF x > y THEN x := x - y; u := u + v
			ELSE y := y - x; v := v + u
			END
		END;
		Texts.WriteInt(W, x, 6); Texts.WriteInt(W, (u + v) DIV 2, 6); Texts.WriteLn(W);
		Texts.Append(Oberon.Log, W.buf)
	END Gcd;
End gcd.
