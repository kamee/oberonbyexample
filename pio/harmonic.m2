MODULE Harmonic;
IMPORT Texts, Oberon;
 VAR y, n: INTEGER;
 x, d, s1, s2: REAL; S: Texts.Scanner;
 W: Texts.Writer;
BEGIN Texts.OpenScanner(S, Oberon.Par.text, Oberon.Par.pos); Texts.Scan(S);
 WHILE S.class = Texts.Int DO
  n:= S.y; Texs.WriteString(W, "n = "); Texts.WriteInt(W, n, 6);
  s1 := 0.0; d := 0.0; y := 0;
  REPEAT
  	d := d + 1.0; INC(y);
	s1 := s1 + 1.0/d;
 UNTIL y >= n;
 Texts.WriteReal(W, s1, 16);
 s2 := 0.0;
 REPEAT
    s2 := s2 + 1.0/d;
	d := d - 1.0; DEC(y)
 UNTIL y = 0;
 Texts.WriteReal(W, s2, 16); Texts.WriteLn(W); Texts.Scan(S)
END;
Texts.Append(Oberon.Log, W.buf)
END Harmonic.
