MODULE hello;
  IMPORT Oberon, Texts;
    VAR W: Texts.Writer;
	BEGIN
	  Texts.OpenWriter(W);
	    Texts.WriteString(W, "Hello."); Texts.WriteLn(W);
		  Texts.Append(Oberon.Log, W.buf)
		  END hello.
