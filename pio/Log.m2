PROCEDURE Log*;
	VAR x, a, b, sum: REAL; S: Texts.Scanner;
BEGIN Texts.OpenScanner(S, Oberon.Par.text, Oberon.Par.pos); Texts.Scan(S);
	WHILE S.class = Texts.Real DO
		x := S.x; Texts.WriteReal(W, x, 15);
		a := x; b := 1.0; sum := 0.0;
		REPEAT
			a := a*a; b := 0.5*b;
			IF a >= 2.0 THEN
				sum := sum + b; a := 0.5*a
			END
		UNTIL b < 1.0E-7;
		Texts.WriteReal(W, sum, 16); Texts.WriteLn(W); Texts.Scan(S)
	END ;
	Texts.Append(Oberon.Log, W.buf)
END Log;
