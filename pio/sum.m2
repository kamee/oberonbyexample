MODULE Sum;
(*
 IMPORT Texts, Oberon;
*)
(*
 PROCEDURE sum(a: ARRAY OF REAL): REAL;
 	VAR i: INTEGER; s: REAL;
 BEGIN s := 0.0;
  FOR i := 0 TO LEN(a)-1 DO s := a[i] + s END ;
  RETURN s
 END sum;
*)
 PROCEDURE square(x: INTEGER; n: INTEGER): INTEGER;
 BEGIN INC(n); RETURN x*x
 END square;
END Sum.
